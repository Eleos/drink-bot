"""
Script_v2 for bot that will be use to test the StaffDrinkBot capabilities.
The final bot will mostly be a copy with different interval time.
"""
import datetime
import logging
import json
from sys import exit
from time import sleep
from telegram.ext import Updater, CallbackContext, CommandHandler

GLOBALS = {}

DATE_DAY_START = 8
DATE_DAY_STOP = 11
HOUR_START = 8
HOUR_STOP = 22 

def parse_json():
    with open("priv.json", 'r') as json_file:
        result = json.load(json_file)
    return result

def time_to_post():
    day = datetime.date.today().day
    if day < DATE_DAY_START or day > DATE_DAY_STOP:
        return False
    hour = datetime.datetime.today().hour
    if hour < HOUR_START or hour > HOUR_STOP:
        return False
    
    return True
    
def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, 
                             text="I'm a bot, I'm here to remind you to drink and eat")

def callsend_message(context):
    context.bot.send_message(chat_id=context.job.context,
                            text='single message test')

def main():
    if not time_to_post():
        exit(0)
    
    GLOBALS = parse_json()
    updater = Updater(token=GLOBALS['token'], use_context=True)
    j_queue = updater.job_queue

    j_queue.run_once(callsend_message, 1, context=GLOBALS['chat_id'])

    updater.start_polling()
    updater.idle()
    
# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    main()