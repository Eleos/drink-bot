"""
Script for bot that will be use to test the StaffDrinkBot capabilities.
The final bot will mostly be a copy with different interval time.
"""
import datetime
import logging
import json
from sys import exit
from time import sleep
from telegram.ext import Updater, CallbackContext, CommandHandler

GLOBALS = {}
TIME_INTERVAL = 120
START_MESSAGE = "Test version:" + "Hello fellow staff, I'm here to remind you to drink and eat regularly"
REGULAR_MESSAGE = "Test version:" + "Hello fellow staff, I'm here to remind you to drink and eat regularly"
SLEEP = True

DATE_DAY_START = 8
DATE_DAY_STOP = 10
HOUR_START = 8
HOUR_STOP = 16

def parse_json():
    with open("priv.json", 'r') as json_file:
        result = json.load(json_file)
    return result

def time_to_post(updater):
    day = datetime.date.today().day
    if day < DATE_DAY_START or day > DATE_DAY_STOP:
        updater.stop()
        exit(0)
    hour = datetime.datetime.today().hour
    if hour < HOUR_START or hour > HOUR_STOP:
        return False

    return True

def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=START_MESSAGE)

def callback_minute(context):
    context.bot.send_message(chat_id=context.job.context, text=REGULAR_MESSAGE)

def main():
    GLOBALS = parse_json()

    updater = Updater(token=GLOBALS['token'], use_context=True)
    job_queue = updater.job_queue
    dispatcher = updater.dispatcher

    # add different jobs ands command handler
    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    # We need to run the first one manually, otherwise the repeating method wait for the time interval without posting the first time
    job_queue.run_once(callback_minute, 1, context=GLOBALS['chat_id'])

    job = job_queue.run_repeating(callback_minute, interval=TIME_INTERVAL, first=0, context=GLOBALS['chat_id'])

    # Start the Bot
    updater.start_polling()

    while(True):
        enable_post = time_to_post(updater)
        job.enabled = enable_post
        if SLEEP:
            sleep(60)

    # Help better CTRL-C the code
    updater.idle()

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    main()