import logging
from telegram.ext import Updater, MessageHandler, Filters

# Basic updater starting stuff
with open("priv") as f:
    token_str = f.readlines()[0].rstrip()
updater = Updater(token=token_str, use_context=True)
dispatcher = updater.dispatcher
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

# Start define handling function
def unknown(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id , text="Sorry, the command you typed is unknown")

unknown_handler = MessageHandler(Filters.command, unknown)
dispatcher.add_handler(unknown_handler)

updater.start_polling()