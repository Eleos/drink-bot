import logging
from telegram.ext import Updater, CommandHandler

# Basic updater starting stuff
with open("priv") as f:
    token_str = f.readlines()[0].rstrip()
updater = Updater(token=token_str, use_context=True)
dispatcher = updater.dispatcher
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

# Start define handling function
def caps(update, context):
    text_caps = ' '.join(context.args).upper()
    context.bot.send_message(chat_id=update.effective_chat.id , text=text_caps)

caps_handler = CommandHandler('caps', caps)
dispatcher.add_handler(caps_handler)

updater.start_polling()