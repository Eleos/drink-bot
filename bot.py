"""
Script for the StaffDrinkBot capabilities.
It answer do 2 things, 
 * tell people to drink every 2 hours in a specific period of time
 * answer to `\start` call to tell them to drink :P
"""
import datetime
import logging
import json
from sys import exit
from time import sleep
from telegram.ext import Updater, CallbackContext, CommandHandler

GLOBALS = {}
TIME_INTERVAL = 30 * 60
SIGNATURE = "\n\nLéonore/H2O"
START_MESSAGE = "Hello fellow staff, je suis ici pour te rappeler de boire et manger regulierement :3" + SIGNATURE + ", Ex-Staff hydratation et bien être"
REGULAR_MESSAGE = "Pensez bien à boire de l'eau, et manger !" + SIGNATURE
SLEEP = True

DATE_DAY_START = 9
DATE_DAY_STOP = 16
HOUR_START = 8
HOUR_STOP = 22

def parse_json():
    """Parse the priv.json file containing all sensible data"""
    with open("priv.json", 'r') as json_file:
        result = json.load(json_file)
    return result

def time_to_post(updater):
    """Verify if it's still the good time to post.
    Will exit (explaining the updater in parameter) if the date is reached"""
    day = datetime.date.today().day
    if day < DATE_DAY_START or day > DATE_DAY_STOP:
        updater.stop()
        exit(0)
    hour = datetime.datetime.today().hour
    if hour < HOUR_START or hour > HOUR_STOP:
        return False

    return True

def start(update, context): 
    """`start` call that will send a gently reminder to drink"""
    context.bot.send_message(chat_id=update.effective_chat.id, text=START_MESSAGE)

def callback_minute(context):
    """Function sending the regular eat and drink message"""
    context.bot.send_message(chat_id=context.job.context, text=REGULAR_MESSAGE)

def main():
    """Run bot"""
    # First get the data from the json
    GLOBALS = parse_json()

    # Create updater and get the job_queue and the dispatcher
    updater = Updater(token=GLOBALS['token'], use_context=True)
    job_queue = updater.job_queue
    dispatcher = updater.dispatcher

    # add different jobs and command handler
    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    # We need to run the first one manually, otherwise the repeating method wait for the time interval without posting the first time
    job_queue.run_once(callback_minute, 1, context=GLOBALS['chat_id'])

    job = job_queue.run_repeating(callback_minute, interval=TIME_INTERVAL, first=0, context=GLOBALS['chat_id'])

    # Start the Bot
    updater.start_polling()

    while(True):
        enable_post = time_to_post(updater)
        job.enabled = enable_post
        if SLEEP:
            sleep(60)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    main()
